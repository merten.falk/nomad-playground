locals {
    dns = [ "169.254.1.1" ]
    datacenters = ["local-dev"]
    nomad_address = "http://169.254.1.1:4646"
    vault_address = "http://169.254.1.1:8200"
    consul_address = "http://169.254.1.1:8500"
}

