resource "vault_kv_secret_v2" "mysql" {
    mount = "secret"
    name = "mysql"
    cas = 1
    delete_all_versions = true
    data_json = jsonencode(
      {
        user = "root"
        password = "password"
      }
    )
}

resource "vault_policy" "mysql-admin" {
    name = "mysql-admin"
    policy = <<EOT
path "secret/data/mysql" {
    capabilities = [ "read" ]
}
EOT
}

resource "vault_kv_secret_v2" "rabbitmq" {
    mount = "secret"
    name = "rabbitmq"
    cas = 1
    delete_all_versions = true
    data_json = jsonencode(
      {
        user = "guest"
        password = "guest"
      }
    )
}

resource "vault_policy" "rabbitmq-guest" {
    name = "rabbitmq-guest"
    policy = <<EOT
path "secret/data/rabbitmq" {
    capabilities = [ "read" ]
}
EOT
}

