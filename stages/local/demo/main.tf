terraform {
  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = "~> 3.11.0"
    }
  }
}

provider "vault" {
    address = local.vault_address
}

module "demo" {
    source = "../../../modules/demo"
    nomad_address = local.nomad_address
    vault_address = local.vault_address
    consul_address = local.consul_address
    datacenters = local.datacenters
    dns = local.dns
    hello-service = {
        image-tag = "latest"
        replication = 4
    }
    greeter-service = {
        image-tag = "latest"
        replication = 2
    }
    rabbitmq = {
        image-tag = "3-management"
    }
}
