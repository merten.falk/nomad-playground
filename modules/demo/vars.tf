variable "nomad_address" {
    type = string
    description = "The nomad cluster to target"
}

variable "vault_address" {
    type = string
    description = "The vault cluster to target"
}

variable "consul_address" {
    type = string
    description = "The consul agent to target"
}

variable "dns" {
    type = list(string)
    description = "The DNS servers that are able to resolve consul addresses"
}

variable "datacenters" {
    type = list(string)
    description = "The datacenters in the region to consider for placement"
}

variable "hello-service" {
    type = object({
        image-tag = string
        replication = number
        }
    )
}

variable "greeter-service" {
    type = object({
        image-tag = string
        replication = number
        }
    )
}

variable "rabbitmq" {
    type = object({
        image-tag = string
    })
}
