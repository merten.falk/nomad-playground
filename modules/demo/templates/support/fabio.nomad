job "fabio" {
  datacenters = ${jsonencode(datacenters)}
  type = "system"

  group "fabio" {
    count = 1

    network {
      port "lb" {
        static = 80
      }

      port "ui" {
        static = 9998
      }
    }

    task "fabio" {
      driver = "docker"

      config {
        image = "fabiolb/fabio"
        args = ["-proxy.addr", ":80", "-ui.color", "deep-purple darken-4"]
        network_mode = "host"
        ports = ["lb", "ui"]
      }

      resources {
          cpu = 200
          memory = 128
      }
    }
  }
}
