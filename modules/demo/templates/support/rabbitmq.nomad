job "rabbitmq" {
  datacenters = ${jsonencode(datacenters)}
  type = "service"

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }

  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }

  group "servers" {
    count = 1

    network {
      port "ui" {
        to = 15672
      }
      port "broker" {
        static = 5672
      }
      dns {
        servers = ["169.254.1.1"]
      }
    }

    service {
      name     = "rabbitmq-ui"
      tags     = ["global", "service", "rabbitmq", "urlprefix-/rabbitmq/ strip=/rabbitmq"]
      port     = "ui"
      provider = "consul"

      check {
        port="ui"
        type = "http"
        name = "Health"
        path = "/"
        header {
          Authorization = ["Basic ${base64encode("guest:guest")}"]
        }
        interval = "10s"
        timeout = "1s"
      }
    }

    service {
      name     = "rabbitmq"
      tags     = ["global", "service", "rabbitmq"]
      port     = "broker"
      provider = "consul"
    }

    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    ephemeral_disk {
      size = 300
    }

    task "server" {
      driver = "docker"

      config {
        image = "rabbitmq:${service.image-tag}"
        ports = ["ui", "broker"]
        auth_soft_fail = true
      }
      env {
        RABBITMQ_DEFAULT_USER ="guest"
        RABBITMQ_DEFAULT_PASS ="guest"
      }
      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
    }
  }
}
