job "mysql" {
  datacenters = ${jsonencode(datacenters)}
  type = "service"

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }

  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }

  group "mysql" {
    count = 1

    network {
      port "db" {
        static = 3306
      }
      dns {
        servers = ["169.254.1.1"]
      }
    }

    service {
      name     = "mysql"
      tags     = ["global", "mysql"]
      port     = "db"
      provider = "consul"
    }

    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    ephemeral_disk {
      size = 300
    }

    task "mysql" {
      driver = "docker"
       env {
        MYSQL_ROOT_PASSWORD = "password"
       }

      config {
        image = "mysql:8.0"
        ports = ["db"]
        auth_soft_fail = true
      }

      resources {
        cpu    = 500
        memory = 512
      }
    }
  }
}
