job "hello-world" {
  datacenters = ${jsonencode(datacenters)}
  type = "service"

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }

  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }

  group "servers" {
    count = ${service.replication}

    network {
      port "http" {
        to = 8080
      }
      dns {
        servers = ["169.254.1.1"]
      }
    }

    service {
      name     = "hello-world"
      tags     = ["global", "service", "urlprefix-/hello-world strip=/hello-world"]
      port     = "http"
      provider = "consul"
      check = {
          port = "http"
          type = "http"
          name = "Health Check"
          path = "/hello/health"
          interval = "10s"
          timeout = "1s"
        }
    }

    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    task "server" {
      driver = "docker"

      config {
        image = "kalleka/hello-world@${image-hash}"
        ports = ["http"]
        auth_soft_fail = true
      }

      resources {
        cpu    = 200 # 500 MHz
        memory = 64 # 256MB
      }
    }
  }
}
