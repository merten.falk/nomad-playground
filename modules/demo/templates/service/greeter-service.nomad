job "greeter" {
  datacenters = ${jsonencode(datacenters)}
  type = "service"
  vault {
    policies = ["mysql-admin", "rabbitmq-guest"]
  }

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }

  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }

  group "worker" {
    count = ${service.replication}

    network {
      dns {
        servers = ["169.254.1.1"]
      }
    }
    service {
      name     = "greeter-worker"
      tags     = ["global", "service"]
      provider = "consul"
    }
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    task "worker" {
      driver = "docker"

      config {
        image = "kalleka/greeter@${image-hash}"
        command = "worker"
        args = ["GreetWorker"]
        auth_soft_fail = true
      }

      resources {
        cpu    = 200 # 200 MHz
        memory = 64 # 64MB
      }
      template {
        data = <<EOH
GREETER_DB_HOST=mysql.service.{{ env "node.datacenter" }}.consul
GREETER_DB_PORT=3306
GREETER_DB_USER={{with secret "secret/data/mysql"}}{{.Data.data.user}}{{end}}
GREETER_DB_PASSWORD={{with secret "secret/data/mysql"}}{{.Data.data.password}}{{end}}
GREETER_BROKER_HOST=rabbitmq.service.{{ env "node.datacenter" }}.consul
GREETER_BROKER_PORT=5672
GREETER_BROKER_USER={{with secret "secret/data/rabbitmq"}}{{.Data.data.user}}{{end}}
GREETER_BROKER_PASSWORD={{with secret "secret/data/rabbitmq"}}{{.Data.data.password}}{{end}}
GREETER_HELLO_URL=http://hello-world.service.{{ env "node.datacenter" }}.consul/hello-world
EOH
        destination = "local/args.env"
        env = true
      }
    }
  }

  group "server" {
    count = ${service.replication}

    network {
      port "http" {
        to = 8080
      }
      dns {
        servers = ["169.254.1.1"]
      }
    }

    service {
      name     = "greeter"
      tags     = ["global", "service", "urlprefix-/greeter strip=/greeter"]
      port     = "http"
      provider = "consul"
      check = {
          port = "http"
          type = "http"
          name = "Health Check"
          path = "/"
          interval = "10s"
          timeout = "1s"
        }
    }

    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    task "server" {
      driver = "docker"
      config {
        image = "kalleka/greeter@${image-hash}"
        command = "server"
        args = ["--bind", "0.0.0.0", "--port", "8080"]
        ports = ["http"]
        auth_soft_fail = true
      }

      template {
        data = <<EOH
GREETER_DB_HOST=mysql.service.{{ env "node.datacenter" }}.consul
GREETER_DB_PORT=3306
GREETER_DB_USER={{with secret "secret/data/mysql"}}{{.Data.data.user}}{{end}}
GREETER_DB_PASSWORD={{with secret "secret/data/mysql"}}{{.Data.data.password}}{{end}}
GREETER_BROKER_HOST=rabbitmq.service.{{ env "node.datacenter" }}.consul
GREETER_BROKER_PORT=5672
GREETER_BROKER_USER={{with secret "secret/data/rabbitmq"}}{{.Data.data.user}}{{end}}
GREETER_BROKER_PASSWORD={{with secret "secret/data/rabbitmq"}}{{.Data.data.password}}{{end}}
EOH
        destination = "local/args.env"
        env = true
      }
      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
    }
  }
}
