resource "nomad_job" "rabbitmq" {
    jobspec = templatefile(
        "${path.module}/templates/support/rabbitmq.nomad",
        {
            datacenters = var.datacenters
            service = var.rabbitmq
        }
    )
}
