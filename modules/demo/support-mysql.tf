resource "nomad_job" "mysql" {
    jobspec = templatefile(
        "${path.module}/templates/support/mysql.nomad",
        {
            datacenters = var.datacenters
        }
    )
}
