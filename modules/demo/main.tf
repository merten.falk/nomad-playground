terraform {
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "~> 1.4.19"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.23.1"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "~> 3.11.0"
    }
  }

}

provider "nomad" {
    address = var.nomad_address
}

provider "vault" {
    address = var.vault_address
}
