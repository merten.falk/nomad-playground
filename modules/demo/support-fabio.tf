resource "nomad_job" "fabio" {
    jobspec = templatefile(
        "${path.module}/templates/support/fabio.nomad",
        {
            datacenters = var.datacenters
        }
    )
}

