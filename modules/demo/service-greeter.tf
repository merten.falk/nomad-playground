data "docker_registry_image" "greeter" {
    name = "kalleka/greeter:latest"
}

resource "nomad_job" "greeter" {
    jobspec = templatefile(
        "${path.module}/templates/service/greeter-service.nomad",
        {
            datacenters = var.datacenters
            image-hash = data.docker_registry_image.greeter.sha256_digest
            service = var.greeter-service
        }
    )
}

