data "docker_registry_image" "hello-world" {
    name = "kalleka/hello-world:latest"
}

resource "nomad_job" "hello-world" {
    jobspec = templatefile(
        "${path.module}/templates/service/hello-world.nomad",
        {
            datacenters = var.datacenters
            image-hash = data.docker_registry_image.hello-world.sha256_digest
            service = var.hello-service
        }
    )
}
