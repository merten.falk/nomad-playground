# Local Development

The default devshell of the nix flake provides a few convenience commands to
setup the local cluster. See `./nix/start-dev.nix` to see the exact commands
that these scripts invoke.

However it is very much recommened that you use the provided nix environment.
The only dependency is nix itself (https://nixos.org/download.html). See
https://direnv.net/ for even better integration with the local shell. Otherwise
invoke the dev shell with `nix develop`.

Running `menu` returns a list of available convenience commands. 

To use them export `DEV_VAULT_TOKEN_ID` with some token. This token will be
picked up by the Vault dev server and used as the root token, which you can
e.g. use to login to the UI. You should also export the same token as
`VAULT_TOKEN`, which will be picked up by other tools to avoid explicitly
passing vault credentials on the commandline.

## Prerequesites

To achieve a local dev setup that resembles the production environment as much
as possible a few conditions must be met on the host.

TL;DR; The loopback device address 127.0.0.1 means different things for running
inside and outside of a docker container. So if our DNS server returns
127.0.0.1 for our Nomad client, services in containers won't be able to use
that address. To circumvent this restriction we're setting up a virtual dummy
interface called "dummy0" with address 169.254.1.1 and bind to that instead.
Now both local services on the host and containerized applications can use that
interface and actually mean the same thing.

For Nixos this is rather simple (`dummyAddress` is 169.254.1.1):

```
networking = {
  firewall = {
      trustedInterfaces = [ "docker0" ];
  };
  interfaces."dummy0" = {
    virtual = true;
    name = "dummy0";
    ipv4 = {
      addresses = [
        {
          address = dummyAddress;
          prefixLength = 32;
        }
      ];
    };
  };
};
```

To make it easy to resolve the consul-addresses from your host machine, also
add dnsmasq:
```
services.dnsmasq = {
  enable = true;
  extraConfig = ''
    server=/local-dev.consul/${dummyAddress}#8600
    listen-address=${dummyAddress}
  '';
};
```

## Setup

To start development:

1. Start Vault in dev mode
```
$ start-vault
```

1. Start Consul in dev mode
```
$ start-consul
```

1. Start nomad in dev mode
```
start-nomad
```
## The Demo

The Demo stack is just a silly application that produces greeting phrases in a
very rube-goldbergy way:

Sending a POST request with json body of the form

```
{"name": "Jane"}
```

to `/greeter`, will internally schedule an
asynchronous task that will in turn request a greeting phrase from another HTTP
service and store the result back in the database.

The components involved are:

1. MySQL database
1. Message broker RabbitMQ
1. Loadbalancer Fabio
1. Greeter Service: HTTP frontend and worker processes. The frontend schedules
   asynchronous tasks for the workers to pick up via RabbitMQ. This service
   stores its state in a MySQL database.
1. Hello Service: Stateless service returns greeting phrases.

To deploy the demo stack locally, go to `./stages/local/demo` and run
`terraform init`, then `terraform plan` and `terraform apply`.

For even more convenience, you can configure your local DNS resolver to forward
requests for the `local-dev.consul` domain to the consul DNS resolver, such
that you can locally resolve the consul addresses. For dnsmasq this would
require adding

```
server=/local-dev.consul/169.254.1.1#8600
```

to your `dnsmasq.conf`. For other dns resolvers refer to
https://developer.hashicorp.com/consul/tutorials/networking/dns-forwarding

# A note on Secrets

Secrets are deliberately NOT managed in this repository. Ie. there is no
mechanism to **write** secrets into Vault from here (with the exception of the
demo and dev environments, which should not contain sensitive data anyway)

It is also be preferred to only **read** secrets directly at the respecitve
use-site (ie. usually from nomad jobspecs). However there might be occasions
where we need to read them from Terraform, so the state file should always be
encrypted na dproperly secured.

The rational for this decision is that contrary to popular opinion secrets
should **not** be versioned alongside the infrastructure, since they tend to
operate on a very different lifecycle. If a secret is compromised, it shouldn't
be used at all, ie. also not accidentally be reverting to an earlier version of
the stack. Not tracking secrets alongside also reduces the risk for leaking
them. At the time it increases the incentive to use short-lived secrets, which
Vault makes easy to do. Ie. ideally there wouldn't be a need for long-lived
secrets at all (and thus writing them down), since all necessary values are
created by Vault directly. 

Until such time we recommend to input secrets into Vault using the CLI or Web
UI.

We acknowledge that this makes it somewhat less convenient to determine which
secrets we actually depend on here, which is why we encourage keeping a
document around listing the paths of any secret along with a short comment on
the kind of content.
