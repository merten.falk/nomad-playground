{ pkgs, ... }:
let
  dev = pkgs.callPackage ./start-dev.nix { };
in
{
  commands = dev ++ [
    {
      package = pkgs.nomad;
    }
    {
      package = pkgs.vault;
    }
    {
      package = pkgs.terraform;
    }
    {
      package = pkgs.consul;
    }
  ];
}
