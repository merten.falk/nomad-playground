{ writeShellScriptBin, nomad, consul, vault-bin }:
let
  # This address circumenvent restricitons around using the loopback address in
  # local dev mode As a rule of thumb, processes on the host itself should
  # target 127.0.0.1, processes in containers should target ${dummyAddress}. If
  # in doubt, use the dummy
  dummyAddress = "169.254.1.1";
  dc = "local-dev";
  start-vault = writeShellScriptBin "start-vault" ''
    set -x
    ${vault-bin}/bin/vault server \
      -dev \
      -dev-listen-address ${dummyAddress}:8200 \
      -dev-root-token-id="$DEV_VAULT_TOKEN_ID" \
      -log-level=TRACE
  '';
  start-consul = writeShellScriptBin "start-consul" ''
    set -x
    ${consul}/bin/consul \
      agent \
      -dev \
      -bind 0.0.0.0 \
      -client "127.0.0.1 ${dummyAddress}" \
      -advertise "127.0.0.1" \
      -log-level INFO \
      -datacenter ${dc}
  '';
  local-nomad-conf = builtins.toFile "nomad.conf" ''
    datacenter = "${dc}"
    vault {
        enabled = true
        address = "http://${dummyAddress}:8200"
    }
    consul {
        address = "${dummyAddress}:8500"
    }
    bind_addr = "0.0.0.0"
    client {
      network_interface = "dummy0"
    }
  '';
  start-nomad = writeShellScriptBin "start-nomad" ''
    set -x
    sudo ${nomad}/bin/nomad agent \
      -dev \
      -log-level INFO \
      -config=${local-nomad-conf} \
      -vault-token=$VAULT_TOKEN
  '';
in
[
  {
    package = start-nomad;
    help = "Start a local nomad";
    category = "dev";
  }
  {
    package = start-consul;
    help = "Start a local consul";
    category = "dev";
  }
  {
    package = start-vault;
    help = "Start a local vault";
    category = "dev";
  }
]
