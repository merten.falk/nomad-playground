{
  description = "A simple dev shell flake";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    devshell.url = "github:numtide/devshell";
    devshell.inputs.nixpkgs.follows = "nixpkgs";
  };
  outputs = { self, nixpkgs, devshell, flake-utils, ... }@inputs:

    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        pkgs' = import nixpkgs {
          inherit system;
          overlays = [ devshell.overlay ];
        };
      in
      {
        devShells = {
          default = pkgs'.devshell.mkShell {
            name = "default";
            imports = [ ./nix/defaultShell.nix ];
          };
        };
      });
}
